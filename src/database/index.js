const sqlite3 = require("sqlite3").verbose();
const database = new sqlite3.Database("database.sqlite");

database.serialize(() => {
  database.run(`CREATE TABLE IF NOT EXISTS USERS(
                stockId INTEGER PRIMARY KEY, 
                name VARCHAR(150),
                description VARCHAR(150),
                quantity INTEGER,
                price INTEGER,
                gst INTEGER,
                total INTEGER)`);
});

export default database;